package tests;

import helpers.CalculateTotalCost;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BasketPage;
import pages.CataloguePage;
import pages.HomePage;
import pages.ProductPage;

import static com.codeborne.selenide.Selenide.open;

public class BasketTests extends TestBase{
    HomePage homePage;
    BasketPage basketPage;
    CataloguePage cataloguePage;
    ProductPage productPage;
    CalculateTotalCost calculateTotalCost;

    @BeforeMethod(alwaysRun = true)
    public void initTest(){
        homePage = new HomePage();
        basketPage = new BasketPage();
        cataloguePage = new CataloguePage();
        productPage = new ProductPage();
        calculateTotalCost = new CalculateTotalCost();
    }

    @Test(enabled = true, groups = "positive")
    @Story("Check cost prise of added book in basket")
    public void checkPriceCostOfAddedBook () throws InterruptedException {
        homePage.selectMenu("Books");
        cataloguePage.selectBookByIndexImage(0);
        Thread.sleep(1000);
        productPage.productInformationIsDisplayed();
        String costSelectedProduct = productPage.getProductCost();
        productPage.addToBasket();
        homePage.viewBasket();
        String costAddedBook = basketPage.getCostBookInBasket();

        Assert.assertEquals(costSelectedProduct, costAddedBook);
    }

    @Test(enabled = true, groups = "positive")
    public void checkNameOfAddedBook () throws InterruptedException {
        homePage.selectMenu("Books");
        cataloguePage.selectBookByIndexImage(0);
        Thread.sleep(1000);
        productPage.productInformationIsDisplayed();
        String nameSelectedProduct = productPage.getProductName();
        productPage.addToBasket();
        homePage.viewBasket();
        String nameAddedBook = basketPage.getNameBookInBasket();

        Assert.assertEquals(nameSelectedProduct, nameAddedBook);
    }

    @Test(enabled = false, groups = "positive")
    public void checkNotificationsAddedBookToBasket (){
        homePage.selectMenu("Books");
        cataloguePage.selectBookByIndexImage(0);
        productPage.productInformationIsDisplayed();
        String nameSelectedProduct = productPage.getProductName();
        String costSelectedProduct = productPage.getProductCost();
        productPage.addToBasket();
        String messageAddedBook = productPage.messageAddedBookInBasket();
        String messageDeferredBenefitOffer = productPage.messageDeferredBenefitInBasket();
        String messageTotalBasketCost = productPage.messageTotalCostInBasket();

        Assert.assertEquals(nameSelectedProduct + " has been added to your basket.", messageAddedBook);
        Assert.assertEquals("Your basket now qualifies for the Deferred benefit offer offer.", messageDeferredBenefitOffer);
        Assert.assertEquals("Your basket total is now " + costSelectedProduct, messageTotalBasketCost);
    }

    @Test(enabled = false, groups = "positive")
    public void addBookFromCatalogueAndProductPages() throws InterruptedException {
        homePage.selectMenu("Books");
        String nameFirstBook = cataloguePage.getNameFirstBookByIndex(0);
        String costFirstBook = cataloguePage.getCostFirstBookByIndex(0);
        cataloguePage.addFirstBookByIndex(0);
        cataloguePage.selectBookByIndexName(0);
        productPage.addToBasket();
        homePage.viewBasket();
        String bookNameInBasket = basketPage.getNameBookInBasket();
        String quantityAddedBook = basketPage.getQuantityBookInBasket();
        String totalPriceSameBooks = basketPage.getTotalPriceSameBooks();
        String expectTotalCost = calculateTotalCost.calculateTotalCostInBasket();

        Assert.assertEquals(nameFirstBook, bookNameInBasket);
        Assert.assertEquals("2", quantityAddedBook);
        Assert.assertEquals(expectTotalCost, totalPriceSameBooks);
    }
}
