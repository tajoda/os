package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.selector.ByText;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class LoginPage {
    SelenideElement loginForm = $("#login_form");
    SelenideElement email = $("#id_login-username");
    SelenideElement password = $("#id_login-password");
    SelenideElement loginButton = $(By.xpath("//button[@name=\"login_submit\"]"));

    SelenideElement registerForm = $("#register_form");
    SelenideElement emailRegister = $("#id_registration-email");
    SelenideElement passwordRegister = $("#id_registration-password1");
    SelenideElement passwordConfirmRegister = $("#id_registration-password2");
    SelenideElement registerButton = $(By.xpath("//button[@name=\"registration_submit\"]"));

    public void loginFormIsVisible (){
        loginForm.shouldBe(Condition.visible);
    }

    public void fillEmail (String s){
        email.click();
        email.setValue(s);
    }

    public void fillPassword(String s) {
        password.click();
        password.setValue(s);
    }

    public void login() {
        loginButton.click();
    }


    public void registerFormIsVisible (){
        registerForm.shouldBe(Condition.visible);
    }

    public void fillEmailRegister(String s) {
        emailRegister.click();
        emailRegister.setValue(s);
    }

    public void fillPasswordRegister(String s) {
        passwordRegister.click();
        passwordRegister.setValue(s);
    }

    public void fillConfirmPasswordRegister(String s) {
        passwordConfirmRegister.click();
        passwordConfirmRegister.setValue(s);
    }

    public void register(){
        registerButton.click();
    }

    public void messageIsDisplayed(String s) {
        $(byText(s)).shouldBe((Condition.visible));
    }

    public void isItLoginPage() {
        Selenide.title().equals("hhttp://selenium1py.pythonanywhere.com/en-gb/accounts/login/");
    }

}
