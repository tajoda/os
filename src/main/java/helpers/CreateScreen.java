package helpers;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import static com.codeborne.selenide.Selenide.screenshot;

public class CreateScreen {
    public static void takeScreenShot() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        String curDate = formatter.format(date);
        String filePath = "screenshots/scr_" + curDate;
        Selenide.screenshot(filePath);
    }

    @Attachment(value = "attachment", type = "image/png", fileExtension = ".png")
    public static byte[] takeScreenShot2() {
        String screenshotAsBase64 = Selenide.screenshot(OutputType.BASE64);
        byte[] decoded = Base64.getDecoder().decode(screenshotAsBase64);
        return decoded;
    }

}
