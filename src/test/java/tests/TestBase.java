package tests;

import com.codeborne.selenide.Configuration;
import helpers.CreateScreen;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;

import static com.codeborne.selenide.Selenide.*;
import static helpers.CreateScreen.*;


public class TestBase {

    @BeforeMethod(alwaysRun = true)
    public void initSelenide(Method m, Object[] p){
        System.setProperty("selenide.browser", "Chrome");
        Configuration.browserSize = "1920x1080";
        //Configuration.headless = true;
        open("http://selenium1py.pythonanywhere.com/en-gb/");
        Configuration.timeout = 3000;
    }

    @AfterMethod(alwaysRun = true)
    public void closeSelenide(ITestResult result, Method method){
        //CreateScreen.takeScreenShot();
        if(!result.isSuccess()){
            takeScreenShot2();
        }
        closeWebDriver();
    }

}
