package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class HomePage {
    SelenideElement loginLink = $("#login_link");
    SelenideElement logoutLink = $("#logout_link");

    public void clickOnLoginLink() {
        loginLink.shouldBe(Condition.visible).click();
    }

    public void messageIsDisplayed(String message) {
        $(byText(message)).shouldBe(Condition.visible);
    }

    public void isItHomePage() {
        Selenide.title().equals("http://selenium1py.pythonanywhere.com/en-gb/");
    }

    public void logout() {
        logoutLink.click();
    }

    public void selectMenu(String value){
        $(By.xpath("//a[contains(text(),'" + value + "')]")).click();
    }

    public void viewBasket() {
        $(byText("View basket")).click();
    }
}
