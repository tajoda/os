package helpers;

import pages.BasketPage;

public class CalculateTotalCost extends BasketPage {
    public String calculateTotalCostInBasket(){
        String t = new String();
        t = this.getPriceBookInBasket().substring(1);
        float cost = Float.parseFloat(t);
        t = this.getQuantityBookInBasket();
        float quantity = Float.parseFloat(t);
        cost *= quantity;
        String str = Float.toString(cost);
        return str = "£" + str;
    }
}
