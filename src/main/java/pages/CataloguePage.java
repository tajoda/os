package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CataloguePage {

    ElementsCollection widgetBookImages = $$("img");
    ElementsCollection widgetBookName = $$(".product_pod>h3");
    ElementsCollection widgetButtonAddToBasket = $$(".btn.btn-primary.btn-block");
    ElementsCollection widgetBookCost = $$(".product_price>p.price_color");

    @Step
    public void selectBookByIndexImage(int i) {
        widgetBookImages.get(i).click();
    }

    @Step
    public void selectBookByIndexName(int i) {
        widgetBookName.get(i).click();
    }

    @Step
    public void addFirstBookByIndex(int i) {
        widgetButtonAddToBasket.get(i).click();
    }

    @Step
    public String getNameFirstBookByIndex(int i) {
        String str = widgetBookName.get(i).text();
        return str;
    }

    @Step
    public String getCostFirstBookByIndex(int i) {
        String str = widgetBookCost.get(i).text();
        return str;
    }


}
