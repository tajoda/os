package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import tests.util.DataProviders;


public class LoginTests extends TestBase {
    HomePage homePage;
    LoginPage loginPage;
    //String emailValid = "sith2@virtualhealth.com";
    //String emailInvalid = "sith12@virtualhealth.com";
    String passwordValid = "sith$92sith$92";
    //String passwordInvalid = "sith$92sith$92123";
    String generatedEmail = generateEmail();

    @BeforeMethod (alwaysRun = true)//показывает, что должен запуститься всегда, если запускаем любую группу
    public void initTest(){
        homePage = new HomePage();//инициализация
        loginPage = new LoginPage();
    }

    public String generateEmail(){
        int min = 1;
        int max = 10000;
        double n = (Math.random() * (max - min) + 1) + min;
        int val = (int)n;
        String generatedEmail = "sith" + val + "@virtualhealth.com";
        return generatedEmail;
    }

    @Test(priority = 1, enabled = false, groups = "positive")
    @Parameters({"emailValid", "passwordValid"})
    public void loginPositive (String emailValid, String passwordValid){
        homePage.clickOnLoginLink();
        loginPage.loginFormIsVisible();
        loginPage.fillEmail(emailValid);
        loginPage.fillPassword(passwordValid);
        loginPage.login();

        homePage.isItHomePage();
        homePage.messageIsDisplayed("Welcome back");
    }

    //@Parameters({"emailInvalid", "passwordInvalid"})
    @Test(priority = 1, enabled = false, dataProviderClass = DataProviders.class, dataProvider = "loginNegativeUsingFile")
    public void loginNegative (String emailInvalid, String passwordInvalid){
        homePage.clickOnLoginLink();
        loginPage.loginFormIsVisible();
        loginPage.fillEmail(emailInvalid);
        loginPage.fillPassword(passwordInvalid);
        loginPage.login();

        loginPage.isItLoginPage();
        loginPage.messageIsDisplayed("Oops! We found some errors");
    }

    @Test(priority = 2, enabled = false, groups = "positive")
    public void registerPositive(){
        homePage.clickOnLoginLink();
        loginPage.registerFormIsVisible();
        //loginPage.fillEmailRegister(emailValid);
        loginPage.fillEmailRegister(generatedEmail);
        loginPage.fillPasswordRegister(passwordValid);
        loginPage.fillConfirmPasswordRegister(passwordValid);
        loginPage.register();

        loginPage.messageIsDisplayed("Thanks for registering!");
        homePage.logout();
    }
}
