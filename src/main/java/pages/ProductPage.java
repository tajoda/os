package pages;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ProductPage {

    @Step
    public void productInformationIsDisplayed() {
        $(byText("Product Information")).shouldBe(Condition.visible);
    }

    @Step
    public String getProductName() {
        return $(".product_main h1").text();
    }

    @Step
    public void addToBasket() {
        $(byText("Add to basket")).click();
    }

    @Step
    public String getProductCost() {
        return $(".price_color").text();
    }

    @Step
    public String messageAddedBookInBasket(){
        String t = new String();
        t = this.getProductName();
        String str = t + " has been added to your basket.";
        return str;
    }

    @Step
    public String messageDeferredBenefitInBasket() {
        String t = $(By.xpath("//strong[contains(text(),\"Deferred benefit offer\")]")).text();
        String str = "Your basket now qualifies for the " + t + " offer.";
        return str;
    }

    @Step
    public String messageTotalCostInBasket() {
        String t = $(".col-sm-6.product_main>p.price_color").text();
        String str = "Your basket total is now " + t;
        return str;
    }
}
