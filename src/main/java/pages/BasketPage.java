package pages;

import static com.codeborne.selenide.Selenide.$;

public class BasketPage {
    public String getNameBookInBasket() {
        return $("div h3 a").text();
    }

    public String getCostBookInBasket() {
        return $(".col-sm-1 .price_color").text();
    }

    public String getQuantityBookInBasket() {
        return $("input[type=number]").getValue();
    }

    public String getPriceBookInBasket(){
        return $("div.col-sm-1>p.price_color.align-right").text();
    }
    public String getTotalPriceSameBooks() {
        return $("div.col-sm-2>p.price_color.align-right").text();
    }
}
